<?php
    function index()
    {
        $string = file_get_contents('input.json');
        $json_a = json_decode($string, true);
        $smsArray = $json_a['sms_list'];
        $priceToReach = $json_a['required_income'];
        $priceList = array();

        foreach ($smsArray as $key => $price) {
            $priceList[$key]['price'] = $price['price'];
            $priceList[$key]['income'] = $price['income'];

        }
        if ($priceToReach > 3) {
            $zmoguiKainuos = getMainValues3($priceToReach, $priceList);
        } else if ($priceToReach > 2 && $priceToReach <= 3) {
            $zmoguiKainuos = getMainValues2($priceToReach, $priceList);
        } else if ($priceToReach > 1 && $priceToReach <= 2) {
            $zmoguiKainuos = getMainValues1($priceToReach, $priceList);
        } else if ($priceToReach <= 1 && $priceToReach > 0.5) {
            $zmoguiKainuos = getMainValues($priceToReach, $priceList);
        } else if ($priceToReach <= 0.5) {
            $zmoguiKainuos['price'] = 1;
            $zmoguiKainuos['number'] = 0.5;
        }

        $naujaKaina = $priceToReach - $zmoguiKainuos['price'];
        if ($naujaKaina > 0) {
            if ($naujaKaina <= 0.41) {
                array_push($zmoguiKainuos['number'], '0.5');
                $zmoguiKainuos['price'] += 0.41;
            } else if ($naujaKaina > 0.41) {
                array_push($zmoguiKainuos['number'], '1');
                $zmoguiKainuos['price'] += 0.96;
            }
        } else {
            print_r('Klaida, patikrinkite ar gerai įvesta sumą');
        }
        print_r($zmoguiKainuos['price']);
        print_r($zmoguiKainuos['number']);
    }


// functions
    function getMainValues($priceToReach, $priceList)
    {
        $numeriai = array();
        $numeriai['number'] = array();
        $count = $priceToReach / $priceList[1]['price'];
        $newPrice = (int)$count * $priceList[1]['price'];
        for ($i = 0; $i < (int)$count; $i++) {
            array_push($numeriai['number'], '1');
            $numeriai['price'] += 0.96;
        }
        $check = $priceToReach - $newPrice;
        if ($check <= 0.5) {
            array_push($numeriai['number'], '0.5');
            $numeriai['price'] += 0.41;

        }
        return $numeriai;
    }

    function getMainValues1($priceToReach, $priceList)
    {
        $numeriai = array();
        $numeriai['number'] = array();
        $count = $priceToReach / $priceList[1]['price'];
        $newPrice = (int)$count * $priceList[1]['price'];
        for ($i = 0; $i < (int)$count; $i++) {
            array_push($numeriai['number'], '1');
            $numeriai['price'] += 0.96;
        }
        $check = $priceToReach - $newPrice;
        if ($check != 0) {
            if ($check <= 0.5) {
                array_push($numeriai['number'], '0.5');
                $numeriai['price'] += 0.41;

            } else if ($check <= 1) {
                array_push($numeriai['number'], '1');
                $numeriai['price'] += 0.96;
            }
        }
        return $numeriai;
    }

    function getMainValues2($priceToReach, $priceList)
    {
        $numeriai = array();
        $numeriai['number'] = array();
        $count = $priceToReach / $priceList[2]['price'];
        $newPrice = (int)$count * $priceList[2]['price'];
        for ($i = 0; $i < (int)$count; $i++) {
            array_push($numeriai['number'], '2');
            $numeriai['price'] += 1.91;
        }
        $check = $priceToReach - $newPrice;
        if ($check != 0) {
            if ($check <= 0.5) {
                array_push($numeriai['number'], '0.5');
                $numeriai['price'] += 0.41;

            } else if ($check <= 1) {
                array_push($numeriai['number'], '1');
                $numeriai['price'] += 0.96;
            }
        }
        return $numeriai;
    }

    function getMainValues3($priceToReach, $priceList)
    {
        $numeriai = array();
        $numeriai['number'] = array();
        $count = $priceToReach / $priceList[3]['price'];
        $newPrice = (int)$count * $priceList[3]['price'];
        for ($i = 0; $i < (int)$count; $i++) {
            array_push($numeriai['number'], '3');
            $numeriai['price'] += 2.90;
        }
        $check = $priceToReach - $newPrice;
        if ($check != 0) {
            if ($check <= 0.5) {
                array_push($numeriai['number'], '0.5');
                $numeriai['price'] += 0.41;

            } else if ($check <= 1) {
                array_push($numeriai['number'], '1');
                $numeriai['price'] += 0.96;
            } else if ($check <= 2) {
                array_push($numeriai['number'], '2');
                $numeriai['price'] += 1.90;
            }

        }
        return $numeriai;

    }


?>